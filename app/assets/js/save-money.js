    $(function() {
        var handleTime = $("#custom-handle-time");
        var handleSalary = $("#custom-handle-salary");
        var handleAttendees = $("#custom-handle-attendees");

        $('#dollar').attr('checked', 'checked');

        function hexFromRGB(r, g, b) {
            var hex = [
                r.toString(16),
                g.toString(16),
                b.toString(16)
            ];
            $.each(hex, function(nr, val) {
                if (val.length === 1) {
                    hex[nr] = "0" + val;
                }
            });
            return hex.join("").toUpperCase();
        }

        function refreshSwatch() {
            var time = $("#mobile-time").slider("value"),
                salary = $("#mobile-salary").slider("value"),
                attendees = $("#mobile-attendees").slider("value"),
                hex = hexFromRGB(red, green, blue);
            $("#swatch").css("background-color", "#" + hex);
        }

        $("#mobile-time").slider({
            orientation: "horizontal",
            range: "min",
            max: 30,
            value: 23,
            create: function() {
                handleTime.text($(this).slider("value"));
            },
            slide: function(event, ui) {
                handleTime.text(ui.value);
                $("#mins").roundSlider("setValue", ui.value);
                calculateLoss();
                if (ui.value <= 5) {
                    $('#mobile-time .ui-slider-handle').addClass('righty');
                } else {
                    $('#mobile-time .ui-slider-handle').removeClass('righty');
                }
            }
        });
        $("#mobile-salary").slider({
            orientation: "horizontal",
            range: "min",
            min: 30000,
            max: 100000,
            value: 63000,
            step: 1000,
            create: function() {
                handleSalary.text("$" + ($(this).slider("value") / 1000) + "K");
            },
            slide: function(event, ui) {
                console.log('ring');
                handleSalary.text("$" + (ui.value / 1000) + "K");
                $("#salary").roundSlider("setValue", ui.value);
                calculateLoss();
                if (ui.value <= 47000) {
                    $('#mobile-salary .ui-slider-handle').addClass('righty');
                } else {
                    $('#mobile-salary .ui-slider-handle').removeClass('righty');
                }
            }
        });
        $("#mobile-attendees").slider({
            orientation: "horizontal",
            range: "min",
            min: 2,
            max: 30,
            value: 3,
            create: function() {
                handleAttendees.text($(this).slider("value"));
            },
            slide: function(event, ui) {
                handleAttendees.text(ui.value);
                $("#attendees").roundSlider("setValue", ui.value);
                calculateLoss();
                if (ui.value <= 5) {
                    $("#mobile-attendees .ui-slider-handle").addClass("righty");
                } else {
                    $("#mobile-attendees .ui-slider-handle").removeClass("righty");
                }
            }
        });
        $("#mobile-time").slider("value", 15);
        $("#mobile-salary").slider("value", 50000);
        $("#mobile-attendees").slider("value", 10);
    });


    $(document).ready(function() {
        var projectedLoss;
        $("#type").roundSlider({
            value: 45,
        });
        $("#mins").roundSlider({
            value: 16,
            min: 1,
            max: 30,
            width: "20",
            radius: "125",
            handleSize: "40",
            sliderType: "min-range",
            tooltipFormat: "changeTooltipMins",
            editableTooltip: false,
            change: "calculateLoss"
        });
        $("#salary").roundSlider({
            value: 73000,
            min: 30000,
            max: 250000,
            width: "20",
            radius: "125",
            step: "1000",
            handleSize: "40",
            sliderType: "min-range",
            tooltipFormat: "changeTooltipSalary",
            editableTooltip: false,
            change: "calculateLoss"
        });
        $("#attendees").roundSlider({
            value: 5,
            min: 2,
            max: 25,
            width: "20",
            radius: "125",
            handleSize: "40",
            sliderType: "min-range",
            tooltipFormat: "changeTooltipAttendees",
            editableTooltip: false,
            change: "calculateLoss"
        });
        /*
        if ($(window).width() < 768) {
        $("#mins").roundSlider({
            radius: "125"
        });
        $("#salary").roundSlider({
            radius: "125"
        });
        $("#attendees").roundSlider({
            radius: "125"
        });
        }
        */
        calculateLoss();
    });
    $("#meetings").selectmenu({
        change: function(event, data) {
            calculateLoss();
        }
    });
    $("#years").selectmenu({
        change: function(event, data) {
            calculateLoss();
        }
    });

    function sliderTypeChanged(e) {
        $("#type").roundSlider({
            sliderType: e.value
        });
    }

    function sliderShapeChanged(e) {
        var options = {
            circleShape: e.value
        };
        $("#mins").roundSlider(options);
        $("#salary").roundSlider(options);
        $("#attendees").roundSlider(options);
    }

    function changeTooltipMins(e) {
        var val = e.value,
            minutes;
        return val + "<div>mins wasted<br /> per meeting</div>";
    }

    function changeTooltipSalary(e) {
        var val = e.value,
            minutes;
        return (val / 1000) + "K" + "<div>average<br /> salary</div>";
    }

    function changeTooltipAttendees(e) {
        var val = e.value,
            minutes;
        return val + "<div>meeting<br /> attendees</div>";
    }

    function calculateLoss() {
        var projectedLoss = $("#mins").roundSlider("option", "value") * ($("#salary").roundSlider("option", "value") / 124000) * $("#attendees").roundSlider("option", "value") * ($("#meetings").val() * 52) * $("#years").val();
        //var projectedLossFormatted = projectedLoss.number(projectedLoss);
        projectedLoss = projectedLoss.toLocaleString(undefined, {
            maximumFractionDigits: 0
        });
        $('input#calculationValue').val('$' + projectedLoss);

        $("#dollar").on("click", function() {
            $('input#calculationValue').val('$' + projectedLoss);
        });
        $("#euro").on("click", function() {
            $('input#calculationValue').val('\u20AC' + projectedLoss);
        });
        $("#dollar-mobile").on("click", function() {
            $('input#calculationValue').val('$' + projectedLoss);
        });
        $("#euro-mobile").on("click", function() {
            $('input#calculationValue').val('\u20AC' + projectedLoss);
        });
    }

    if ($(window).width() < 1240) {
        $('div.col-9').removeClass('col-9');
        $('.logo-corner').attr('src', 'assets/img/mobile/barco-logo-mobile.png');
        $('.save-money-cta .btn-next').attr('href', 'select-device.html');
    }