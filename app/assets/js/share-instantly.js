var anim_script = document.createElement('script');
if ($(window).width() < 768) {
    anim_script.setAttribute('src', '/assets/js/share_mobile.js?1516218676713');
} else {
    anim_script.setAttribute('src', 'assets/js/share_desktop_700x700.js?1515792360432')
}
document.head.appendChild(anim_script);

var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;

function init() {
    canvas = document.getElementById("canvas");
    anim_container = document.getElementById("animation_container");
    dom_overlay_container = document.getElementById("dom_overlay_container");
    var comp = AdobeAn.getComposition("C7460226EE5C4A41A91D0FC75828EA79");
    var lib = comp.getLibrary();
    var loader = new createjs.LoadQueue(false);
    loader.addEventListener("fileload", function(evt) {
        handleFileLoad(evt, comp)
    });
    loader.addEventListener("complete", function(evt) {
        handleComplete(evt, comp)
    });
    var lib = comp.getLibrary();
    loader.loadManifest(lib.properties.manifest);
    // Text for animation
    setTimeout(function() {
        $('#dom_overlay_container .animation-text-target').text('For instant presentations');
        $('#dom_overlay_container .animation-text-target').fadeIn();
        setTimeout(function() {
            $('#dom_overlay_container .animation-text-target').fadeOut();
        }, 2500);
    }, 1000);
    setTimeout(function() {
        $('#dom_overlay_container .animation-text-target').text('For instant screen sharing');
        $('#dom_overlay_container .animation-text-target').fadeIn();
        setTimeout(function() {
            $('#dom_overlay_container .animation-text-target').fadeOut();
        }, 3000);
    }, 4400);
    setTimeout(function() {
        $('#dom_overlay_container .animation-text-target').text('For multi-device collaboration');
        $('#dom_overlay_container .animation-text-target').fadeIn();
        setTimeout(function() {
            $('#dom_overlay_container .animation-text-target').fadeOut();
        }, 3000);
    }, 8000);
    setTimeout(function() {
        $('#dom_overlay_container .animation-text-target').text('For more secure communications');
        $('#dom_overlay_container .animation-text-target').fadeIn();
        setTimeout(function() {
            $('#dom_overlay_container .animation-text-target').fadeOut();
        }, 2500);
    }, 11500);
    setTimeout(function() {
        $('#dom_overlay_container .animation-button-target').fadeIn();
    }, 14900);
    setTimeout(function() {
        $('#dom_overlay_container .animation-text-target').text('To save space');
        $('#dom_overlay_container .animation-text-target').addClass('final-text');
        $('#dom_overlay_container .animation-text-target').fadeIn();
    }, 14900);


}

function handleFileLoad(evt, comp) {
    var images = comp.getImages();
    if (evt && (evt.item.type == "image")) {
        images[evt.item.id] = evt.result;
    }
}

function handleComplete(evt, comp) {
    //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
    var lib = comp.getLibrary();
    var ss = comp.getSpriteSheet();
    var queue = evt.target;
    var ssMetadata = lib.ssMetadata;
    for (i = 0; i < ssMetadata.length; i++) {
        ss[ssMetadata[i].name] = new createjs.SpriteSheet({
            "images": [queue.getResult(ssMetadata[i].name)],
            "frames": ssMetadata[i].frames
        })
    }
    if ($(window).width() < 768) {
        exportRoot = new lib.share_mobile();
    } else {
        exportRoot = new lib.share_desktop_700x700();
    }

    //exportRootMobile = new lib.share_mobile();
    stage = new lib.Stage(canvas);
    stage.enableMouseOver();
    //Registers the "tick" event listener.
    fnStartAnimation = function() {
        stage.addChild(exportRoot);
        createjs.Ticker.setFPS(lib.properties.fps);
        createjs.Ticker.addEventListener("tick", stage);
    }
    //Code to support hidpi screens and responsive scaling.
    function makeResponsive(isResp, respDim, isScale, scaleType) {
        var lastW, lastH, lastS = 1;
        window.addEventListener('resize', resizeCanvas);
        resizeCanvas();

        function resizeCanvas() {
            var w = lib.properties.width,
                h = lib.properties.height;
            var iw = window.innerWidth,
                ih = window.innerHeight;
            var pRatio = window.devicePixelRatio || 1,
                xRatio = iw / w,
                yRatio = ih / h,
                sRatio = 1;
            if (isResp) {
                if ((respDim == 'width' && lastW == iw) || (respDim == 'height' && lastH == ih)) {
                    sRatio = lastS;
                } else if (!isScale) {
                    if (iw < w || ih < h)
                        sRatio = Math.min(xRatio, yRatio);
                } else if (scaleType == 1) {
                    sRatio = Math.min(xRatio, yRatio);
                } else if (scaleType == 2) {
                    sRatio = Math.max(xRatio, yRatio);
                }
            }
            canvas.width = w * pRatio * sRatio;
            canvas.height = h * pRatio * sRatio;
            console.log($(window).width());
            if ($(window).width() >= 768) {
                canvas.style.width = '700px';
                canvas.style.height = '700px';
            } 
            else if($(window).width() == 375) {  //iPhone
                canvas.style.width = dom_overlay_container.style.width = anim_container.style.width = '375px'; //w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = '601px' // w*sRatio+'px';
            }
            else if($(window).width() == 414) {  //iPhone 8+
                canvas.style.width = dom_overlay_container.style.width = anim_container.style.width = '414px'; //w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = '663px' // w*sRatio+'px';
            }
            else if($(window).width() == 360) {  //samsung s8
                canvas.style.width = dom_overlay_container.style.width = anim_container.style.width = '360px'; //w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = '585px' // w*sRatio+'px';
            } else {
                canvas.style.width = dom_overlay_container.style.width = anim_container.style.width = '100%'; //w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = '100%;' // w*sRatio+'px';
            }
            stage.scaleX = pRatio * sRatio;
            stage.scaleY = pRatio * sRatio;
            lastW = iw;
            lastH = ih;
            lastS = sRatio;
            stage.tickOnUpdate = false;
            stage.update();
            stage.tickOnUpdate = true;
        }
    }
    makeResponsive(true, 'both', true, 1);
    AdobeAn.compositionLoaded(lib.properties.id);
    fnStartAnimation();
}
if ($(window).width() < 1025) {
    //console.log('bing');
  $('div.col-9').removeClass('col-9');
  $('.logo-corner').attr('src', 'assets/img/mobile/barco-logo-mobile.png');
  $('.main .btn-hdr').attr('href', 'select-device.html');

}

