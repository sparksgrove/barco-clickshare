(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.illu_CSE200_black_CSE200 = function() {
	this.initialize(img.illu_CSE200_black_CSE200);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,97,69);


(lib.Illu_TV_1Device_Mobile = function() {
	this.initialize(img.Illu_TV_1Device_Mobile);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,311,187);


(lib.Illu_TV_2Devices_Mobile = function() {
	this.initialize(img.Illu_TV_2Devices_Mobile);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,311,187);


(lib.Illu_TV_4Devices_Mobile = function() {
	this.initialize(img.Illu_TV_4Devices_Mobile);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,311,187);


(lib.Illu_TV_Black_Mobile = function() {
	this.initialize(img.Illu_TV_Black_Mobile);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,311,187);


(lib.LLaptopon = function() {
	this.initialize(img.LLaptopon);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,201,125);


(lib.LPhoneon = function() {
	this.initialize(img.LPhoneon);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,84,71);


(lib.LaptopLoff = function() {
	this.initialize(img.LaptopLoff);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,204,132);


(lib.LaptopRoff = function() {
	this.initialize(img.LaptopRoff);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,193,125);


(lib.Lockicon = function() {
	this.initialize(img.Lockicon);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,34,37);


(lib.RLaptopon = function() {
	this.initialize(img.RLaptopon);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,190,118);


(lib.RTableton = function() {
	this.initialize(img.RTableton);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,93,70);


(lib.Symbol12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#D70000","#F00000"],[0,1],0,20.5,0,-20.4).s().p("AsrDIQghAAAAgjIAAlJQAAgjAhAAIZXAAQAhAAAAAjIAAFJQAAAjghAAg");
	this.shape.setTransform(84.5,20);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,169,40);


(lib.Symbol11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Lockicon();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,34,37);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["rgba(144,186,227,0.149)","rgba(144,186,227,0.6)"],[0.416,1],0,0,0,0,0,62.5).s().p("AmyGzQi0i0AAj/QAAj+C0i0QC0i0D+AAQD/AAC0C0QC0C0AAD+QAAD/i0C0Qi0C0j/AAQj+AAi0i0g");
	this.shape.setTransform(61.5,61.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,123,123);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Illu_TV_4Devices_Mobile();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,311,187);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.LPhoneon();
	this.instance.parent = this;
	this.instance.setTransform(0,-20);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-20,84,71);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.RTableton();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,93,70);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Illu_TV_2Devices_Mobile();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,311,187);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.RLaptopon();
	this.instance.parent = this;
	this.instance.setTransform(3,2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(3,2,190,118);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.LaptopRoff();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,193,125);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Illu_TV_1Device_Mobile();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,311,187);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Illu_TV_Black_Mobile();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,311,187);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.LLaptopon();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,201,125);


// stage content:
(lib.share_mobile = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_256 = function() {
		this.button_1.addEventListener("click", fl_ClickToGoToWebPage);
		
		function fl_ClickToGoToWebPage() {
			window.open("http://barco.demo.sparksgrove.com/save-space.html", "_self");
		}
	}
	this.frame_266 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(256).call(this.frame_256).wait(10).call(this.frame_266).wait(1));

	// mask (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_168 = new cjs.Graphics().p("ALUXQIBWjVIYWJ0IhXDWg");
	var mask_graphics_169 = new cjs.Graphics().p("AKkW0IBWjVIYVJ0IhWDVg");
	var mask_graphics_170 = new cjs.Graphics().p("AJzWYIBWjWIYWJ1IhWDVg");
	var mask_graphics_171 = new cjs.Graphics().p("AJDV7IBWjVIYWJ0IhXDWg");
	var mask_graphics_172 = new cjs.Graphics().p("AITVfIBWjVIYVJ0IhWDVg");
	var mask_graphics_173 = new cjs.Graphics().p("AHiVDIBWjWIYWJ1IhWDVg");
	var mask_graphics_174 = new cjs.Graphics().p("AGyUmIBWjVIYWJ0IhXDWg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(168).to({graphics:mask_graphics_168,x:236.8,y:211.7}).wait(1).to({graphics:mask_graphics_169,x:231.9,y:208.8}).wait(1).to({graphics:mask_graphics_170,x:227.1,y:206}).wait(1).to({graphics:mask_graphics_171,x:222.3,y:203.2}).wait(1).to({graphics:mask_graphics_172,x:217.4,y:200.3}).wait(1).to({graphics:mask_graphics_173,x:212.6,y:197.5}).wait(1).to({graphics:mask_graphics_174,x:207.8,y:194.7}).wait(93));

	// line 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.498)").s().p("ADkCZQgGgFAAgJQAAgIAGgGQAFgGAJAAQAJAAAFAGQAGAGAAAIQAAAJgGAFQgFAGgJAAQgJAAgFgGgACdByQgFgFAAgJQAAgIAFgGQAGgGAJAAQAIAAAGAGQAGAGAAAIQAAAJgGAFQgGAGgIAAQgJAAgGgGgABYBLQgFgGAAgIQAAgJAFgGQAHgFAIAAQAJAAAFAFQAGAGAAAJQAAAIgGAGQgFAGgJAAQgIAAgHgGgAAUAjQgGgGAAgIQAAgJAGgGQAFgFAJAAQAIAAAGAFQAGAGAAAJQAAAIgGAGQgGAGgIAAQgJAAgFgGgAgwgEQgGgGAAgIQAAgJAGgGQAFgFAJAAQAIAAAGAFQAGAGAAAJQAAAIgGAGQgGAFgIAAQgJAAgFgFgAh2gsQgFgGAAgIQAAgJAFgGQAHgFAIAAQAJAAAFAFQAGAGAAAJQAAAIgGAGQgFAGgJAAQgIAAgHgGgAi6hUQgGgGAAgIQAAgJAGgGQAFgFAJAAQAIAAAGAFQAGAGAAAJQAAAIgGAGQgGAGgIAAQgJAAgFgGgAkAh8QgFgGAAgIQAAgJAFgGQAGgFAJAAQAIAAAGAFQAGAGAAAJQAAAIgGAGQgGAGgIAAQgJAAgGgGg");
	this.shape.setTransform(286.2,331.9);
	this.shape._off = true;

	var maskedShapeInstanceList = [this.shape];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(168).to({_off:false},0).wait(99));

	// mask (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	var mask_1_graphics_161 = new cjs.Graphics().p("As1dMIYVp0IBWDVI4VJ1g");
	var mask_1_graphics_162 = new cjs.Graphics().p("As1c2IYVp0IBWDVI4VJ0g");
	var mask_1_graphics_163 = new cjs.Graphics().p("As1cfIYVp0IBWDVI4VJ1g");
	var mask_1_graphics_164 = new cjs.Graphics().p("As1cJIYVp0IBWDVI4VJ0g");
	var mask_1_graphics_165 = new cjs.Graphics().p("As1byIYVp0IBWDVI4VJ1g");
	var mask_1_graphics_166 = new cjs.Graphics().p("As1bcIYVp0IBWDVI4VJ0g");
	var mask_1_graphics_167 = new cjs.Graphics().p("AsvbFIYUp0IBWDVI4UJ1g");

	this.timeline.addTween(cjs.Tween.get(mask_1).to({graphics:null,x:0,y:0}).wait(161).to({graphics:mask_1_graphics_161,x:23.3,y:208.2}).wait(1).to({graphics:mask_1_graphics_162,x:33.3,y:205.9}).wait(1).to({graphics:mask_1_graphics_163,x:43.3,y:203.7}).wait(1).to({graphics:mask_1_graphics_164,x:53.3,y:201.4}).wait(1).to({graphics:mask_1_graphics_165,x:63.3,y:199.2}).wait(1).to({graphics:mask_1_graphics_166,x:73.3,y:196.9}).wait(1).to({graphics:mask_1_graphics_167,x:82.7,y:194.7}).wait(100));

	// line 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(0,0,0,0.498)").s().p("AkQB4QgGgFAAgJQAAgIAGgGQAFgGAJAAQAIAAAGAGQAGAGAAAIQAAAJgGAFQgGAGgIAAQgJAAgFgGgAjGBZQgGgFAAgJQAAgIAGgGQAFgGAJAAQAJAAAFAGQAGAGAAAIQAAAJgGAFQgFAGgJAAQgJAAgFgGgAh9A7QgFgFAAgJQAAgIAFgGQAHgGAIAAQAIAAAGAGQAGAGAAAIQAAAJgGAFQgGAGgIAAQgIAAgHgGgAgzAcQgFgFAAgJQAAgIAFgGQAHgFAIAAQAJAAAFAFQAGAGAAAIQAAAJgGAFQgFAGgJAAQgIAAgHgGgAAXgBQgGgFAAgJQAAgIAGgGQAFgGAJAAQAIAAAGAGQAGAGAAAIQAAAJgGAFQgGAFgIAAQgJAAgFgFgABhgfQgGgGAAgIQAAgJAGgGQAFgFAJAAQAJAAAFAFQAGAGAAAJQAAAIgGAGQgFAGgJAAQgJAAgFgGgACqg9QgFgGAAgIQAAgJAFgGQAGgFAJAAQAIAAAGAFQAGAGAAAJQAAAIgGAGQgGAGgIAAQgJAAgGgGgAD0hbQgFgGAAgIQAAgJAFgGQAHgFAIAAQAIAAAGAFQAGAGAAAJQAAAIgGAGQgGAGgIAAQgIAAgHgGg");
	this.shape_1.setTransform(130.1,327.6);
	this.shape_1._off = true;

	var maskedShapeInstanceList = [this.shape_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(161).to({_off:false},0).wait(106));

	// mask (mask)
	var mask_2 = new cjs.Shape();
	mask_2._off = true;
	var mask_2_graphics_96 = new cjs.Graphics().p("APTXXIDkgcIDMaDIjkAcg");
	var mask_2_graphics_97 = new cjs.Graphics().p("AO/VNIDkgcIDMaDIjkAcg");
	var mask_2_graphics_98 = new cjs.Graphics().p("AOrTDIDkgcIDMaDIjkAcg");
	var mask_2_graphics_99 = new cjs.Graphics().p("AOXQ5IDkgcIDMaDIjkAcg");
	var mask_2_graphics_100 = new cjs.Graphics().p("AODOvIDkgcIDMaDIjkAcg");
	var mask_2_graphics_101 = new cjs.Graphics().p("ANvMlIDkgcIDMaEIjkAbg");
	var mask_2_graphics_102 = new cjs.Graphics().p("ANbKbIDkgcIDMaEIjkAbg");

	this.timeline.addTween(cjs.Tween.get(mask_2).to({graphics:null,x:0,y:0}).wait(96).to({graphics:mask_2_graphics_96,x:141.1,y:316.2}).wait(1).to({graphics:mask_2_graphics_97,x:139.1,y:302.4}).wait(1).to({graphics:mask_2_graphics_98,x:137.1,y:288.6}).wait(1).to({graphics:mask_2_graphics_99,x:135.1,y:274.8}).wait(1).to({graphics:mask_2_graphics_100,x:133.1,y:261}).wait(1).to({graphics:mask_2_graphics_101,x:131.1,y:247.2}).wait(1).to({graphics:mask_2_graphics_102,x:129.1,y:233.4}).wait(165));

	// line 1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.498)").s().p("ABZKuQgFgGAAgIQAAgJAFgGQAGgFAIAAQAJAAAGAFQAFAGAAAJQAAAIgFAGQgGAGgJAAQgIAAgGgGgABNJeQgGgFABgJQgBgIAGgGQAGgGAJAAQAIAAAFAGQAHAGAAAIQAAAJgHAFQgFAGgIAAQgJAAgGgGgABAIPQgFgFgBgJQABgIAFgGQAGgGAIAAQAJAAAGAGQAFAGABAIQgBAJgFAFQgGAGgJAAQgIAAgGgGgAA0HAQgGgFABgJQgBgIAGgGQAGgGAJAAQAIAAAGAGQAFAGAAAIQAAAJgFAFQgGAGgIAAQgJAAgGgGgAAnFxQgGgFAAgJQAAgIAGgGQAGgGAJAAQAIAAAFAGQAHAGAAAIQAAAJgHAFQgFAGgIAAQgJAAgGgGgAAbEiQgFgFAAgJQAAgIAFgGQAGgGAIAAQAJAAAGAGQAFAGAAAIQAAAJgFAFQgGAGgJAAQgIAAgGgGgAAODTQgGgFABgJQgBgIAGgGQAGgGAJAAQAIAAAFAGQAHAGgBAIQABAJgHAFQgFAGgIAAQgJAAgGgGgAACCEQgEgFgBgJQABgIAEgGQAGgGAIAAQAJAAAFAGQAGAGABAIQgBAJgGAFQgFAGgJAAQgIAAgGgGgAgJA1QgFgFAAgJQAAgIAFgGQAGgGAHAAQAJAAAGAGQAFAGAAAIQAAAJgFAFQgGAGgJAAQgHAAgGgGgAgVgZQgGgFABgJQgBgIAGgGQAGgGAJAAQAHAAAFAGQAHAGgBAIQABAJgHAFQgFAGgHAAQgJAAgGgGgAghhoQgFgFgBgJQABgIAFgGQAGgGAIAAQAJAAAGAGQAEAGABAIQgBAJgEAFQgGAGgJAAQgIAAgGgGgAgti3QgFgFAAgJQAAgIAFgGQAGgGAIAAQAJAAAGAGQAFAGAAAIQAAAJgFAFQgGAGgJAAQgIAAgGgGgAg5kGQgGgFABgJQgBgIAGgGQAGgGAJAAQAIAAAFAGQAHAGAAAIQAAAJgHAFQgFAGgIAAQgJAAgGgGgAhFlVQgFgFgBgJQABgIAFgGQAGgGAIAAQAJAAAFAGQAGAGABAIQgBAJgGAFQgFAGgJAAQgIAAgGgGgAhRmkQgFgFAAgJQAAgIAFgGQAGgGAIAAQAJAAAGAGQAFAGAAAIQAAAJgFAFQgGAGgJAAQgIAAgGgGgAhdnzQgGgFABgJQgBgIAGgGQAGgGAJAAQAIAAAFAGQAHAGgBAIQABAJgHAFQgFAGgIAAQgJAAgGgGgAhppCQgFgFgBgJQABgIAFgGQAGgGAIAAQAJAAAFAGQAGAGABAIQgBAJgGAFQgFAGgJAAQgIAAgGgGgAh1qRQgFgFAAgJQAAgIAFgGQAGgGAIAAQAJAAAGAGQAFAGAAAIQAAAJgFAFQgGAGgJAAQgIAAgGgGg");
	this.shape_2.setTransform(236.4,394.1);
	this.shape_2._off = true;

	var maskedShapeInstanceList = [this.shape_2];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_2;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(96).to({_off:false},0).wait(171));

	// mask (mask)
	var mask_3 = new cjs.Shape();
	mask_3._off = true;
	var mask_3_graphics_32 = new cjs.Graphics().p("EAHkAw+IDL6DIDlAcIjMaDg");
	var mask_3_graphics_33 = new cjs.Graphics().p("EAH4Au0IDM6DIDlAcIjMaDg");
	var mask_3_graphics_34 = new cjs.Graphics().p("EAINAsqIDM6DIDkAcIjLaDg");
	var mask_3_graphics_35 = new cjs.Graphics().p("EAIiAqgIDM6DIDkAcIjLaDg");
	var mask_3_graphics_36 = new cjs.Graphics().p("EAI3AoWIDM6DIDkAcIjMaDg");
	var mask_3_graphics_37 = new cjs.Graphics().p("EAJMAmNIDM6EIDkAcIjMaDg");
	var mask_3_graphics_38 = new cjs.Graphics().p("EAJhAkDIDM6EIDkAcIjMaDg");

	this.timeline.addTween(cjs.Tween.get(mask_3).to({graphics:null,x:0,y:0}).wait(32).to({graphics:mask_3_graphics_32,x:91.6,y:316.2}).wait(1).to({graphics:mask_3_graphics_33,x:93.7,y:302.4}).wait(1).to({graphics:mask_3_graphics_34,x:95.7,y:288.6}).wait(1).to({graphics:mask_3_graphics_35,x:97.8,y:274.8}).wait(1).to({graphics:mask_3_graphics_36,x:99.9,y:261}).wait(1).to({graphics:mask_3_graphics_37,x:102,y:247.2}).wait(1).to({graphics:mask_3_graphics_38,x:104.1,y:233.4}).wait(229));

	// line 1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("rgba(0,0,0,0.498)").s().p("Ah1KuQgGgGAAgIQAAgJAGgGQAGgFAJAAQAIAAAFAFQAHAGAAAJQAAAIgHAGQgFAGgIAAQgJAAgGgGgAhpJeQgGgFABgJQgBgIAGgGQAGgGAIAAQAJAAAGAGQAFAGAAAIQAAAJgFAFQgGAGgJAAQgIAAgGgGgAhcIPQgFgFgBgJQABgIAFgGQAGgGAIAAQAJAAAFAGQAGAGABAIQgBAJgGAFQgFAGgJAAQgIAAgGgGgAhQHAQgGgFABgJQgBgIAGgGQAGgGAJAAQAIAAAFAGQAHAGgBAIQABAJgHAFQgFAGgIAAQgJAAgGgGgAhDFxQgFgFAAgJQAAgIAFgGQAGgGAIAAQAJAAAGAGQAFAGAAAIQAAAJgFAFQgGAGgJAAQgIAAgGgGgAg3EiQgGgFAAgJQAAgIAGgGQAGgGAJAAQAIAAAFAGQAHAGAAAIQAAAJgHAFQgFAGgIAAQgJAAgGgGgAgqDTQgGgFABgJQgBgIAGgGQAGgGAJAAQAIAAAGAGQAFAGAAAIQAAAJgFAFQgGAGgIAAQgJAAgGgGgAgeCEQgFgFgBgJQABgIAFgGQAGgGAIAAQAJAAAGAGQAEAGABAIQgBAJgEAFQgGAGgJAAQgIAAgGgGgAgSA1QgGgFAAgJQAAgIAGgGQAGgGAJAAQAHAAAFAGQAHAGAAAIQAAAJgHAFQgFAGgHAAQgJAAgGgGgAgGgZQgGgFABgJQgBgIAGgGQAGgGAHAAQAJAAAGAGQAFAGAAAIQAAAJgFAFQgGAGgJAAQgHAAgGgGgAAFhoQgFgFAAgJQAAgIAFgGQAGgGAIAAQAJAAAGAGQAFAGABAIQgBAJgFAFQgGAGgJAAQgIAAgGgGgAARi3QgGgFAAgJQAAgIAGgGQAGgGAJAAQAIAAAFAGQAHAGAAAIQAAAJgHAFQgFAGgIAAQgJAAgGgGgAAdkGQgGgFABgJQgBgIAGgGQAGgGAIAAQAJAAAGAGQAFAGAAAIQAAAJgFAFQgGAGgJAAQgIAAgGgGgAAplVQgFgFgBgJQABgIAFgGQAGgGAIAAQAJAAAGAGQAFAGABAIQgBAJgFAFQgGAGgJAAQgIAAgGgGgAA1mkQgGgFAAgJQAAgIAGgGQAGgGAJAAQAIAAAFAGQAHAGAAAIQAAAJgHAFQgFAGgIAAQgJAAgGgGgABBnzQgGgFABgJQgBgIAGgGQAGgGAIAAQAJAAAGAGQAFAGAAAIQAAAJgFAFQgGAGgJAAQgIAAgGgGgABNpCQgFgFgBgJQABgIAFgGQAGgGAIAAQAJAAAGAGQAFAGABAIQgBAJgFAFQgGAGgJAAQgIAAgGgGgABZqRQgGgFAAgJQAAgIAGgGQAGgGAJAAQAIAAAFAGQAHAGAAAIQAAAJgHAFQgFAGgIAAQgJAAgGgGg");
	this.shape_3.setTransform(184.7,394.1);
	this.shape_3._off = true;

	var maskedShapeInstanceList = [this.shape_3];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_3;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(32).to({_off:false},0).wait(235));

	// mask (mask)
	var mask_4 = new cjs.Shape();
	mask_4._off = true;
	mask_4.graphics.p("A2XM0IAA5nMAsvAAAIAAZng");
	mask_4.setTransform(204.3,125.6);

	// big screen C
	this.instance = new lib.Symbol9("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(207.5,129.5,1,1,0,0,0,158.5,96.5);
	this.instance.alpha = 0;
	this.instance._off = true;

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_4;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(177).to({_off:false},0).to({alpha:1},6).wait(84));

	// big screen B
	this.instance_1 = new lib.Symbol6("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(207.5,129.5,1,1,0,0,0,158.5,96.5);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_4;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(105).to({_off:false},0).to({alpha:1},6).wait(156));

	// big screen A
	this.instance_2 = new lib.Symbol3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(207.5,129.5,1,1,0,0,0,158.5,96.5);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	var maskedShapeInstanceList = [this.instance_2];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_4;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(41).to({_off:false},0).to({alpha:1},6).wait(220));

	// big screen
	this.instance_3 = new lib.Symbol2("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(207.5,129.5,1,1,0,0,0,158.5,96.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(267));

	// mask (mask)
	var mask_5 = new cjs.Shape();
	mask_5._off = true;
	mask_5.graphics.p("An6lBIO6hgIA8LZIvUBpg");
	mask_5.setTransform(48.3,435.7);

	// Layer_3
	this.instance_4 = new lib.Symbol1("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(102,451,1,1,0,0,0,102,66);
	this.instance_4.alpha = 0;
	this.instance_4._off = true;

	var maskedShapeInstanceList = [this.instance_4];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_5;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(26).to({_off:false},0).to({alpha:1},5).wait(236));

	// left laptop
	this.instance_5 = new lib.LaptopLoff();
	this.instance_5.parent = this;
	this.instance_5.setTransform(0,385);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(267));

	// tablet
	this.instance_6 = new lib.Symbol7("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(469.5,353.5,1,1,0,0,0,49.5,37.5);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(158).to({_off:false},0).to({x:359.5,y:343.5},5).wait(104));

	// phone
	this.instance_7 = new lib.Symbol8("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(-42.5,362.5,1,1,0,0,0,43.5,20.5);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(155).to({_off:false},0).to({x:57.5,y:352.5},5).wait(107));

	// mask (mask)
	var mask_6 = new cjs.Shape();
	mask_6._off = true;
	mask_6.graphics.p("AnfFUIAjrZIOcA3IAALUg");
	mask_6.setTransform(374,432);

	// Layer_5
	this.instance_8 = new lib.Symbol5("synched",0);
	this.instance_8.parent = this;
	this.instance_8.setTransform(318.5,446.5,1,1,0,0,0,96.5,62.5);
	this.instance_8.alpha = 0;
	this.instance_8._off = true;

	var maskedShapeInstanceList = [this.instance_8];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_6;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(90).to({_off:false},0).to({alpha:1},4).wait(173));

	// right laptop
	this.instance_9 = new lib.Symbol4("synched",0);
	this.instance_9.parent = this;
	this.instance_9.setTransform(318.5,446.5,1,1,0,0,0,96.5,62.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(267));

	// device
	this.instance_10 = new lib.illu_CSE200_black_CSE200();
	this.instance_10.parent = this;
	this.instance_10.setTransform(161,253);

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(267));

	// lock
	this.instance_11 = new lib.Symbol11("synched",0);
	this.instance_11.parent = this;
	this.instance_11.setTransform(210,272.5,1,1,0,0,0,17,18.5);
	this.instance_11.alpha = 0;
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(232).to({_off:false},0).to({y:262.5,alpha:1},5).wait(30));

	// CTA
	this.button_1 = new lib.Symbol12();
	this.button_1.name = "button_1";
	this.button_1.parent = this;
	this.button_1.setTransform(216.5,564.4,1,1,0,0,0,84.5,20);
	this.button_1._off = true;
	new cjs.ButtonHelper(this.button_1, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get(this.button_1).wait(252).to({_off:false},0).wait(2).to({scaleX:0.9,scaleY:0.9},0).wait(2).to({scaleX:1,scaleY:1},0).wait(11));

	// shield
	this.instance_12 = new lib.Symbol10("synched",0);
	this.instance_12.parent = this;
	this.instance_12.setTransform(209.5,290.5,0.1,0.1,0,0,0,62,62);
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(226).to({_off:false},0).to({regX:61.6,regY:61.6,scaleX:1.1,scaleY:1.1,x:209.6},5).wait(1).to({regX:61.5,regY:61.5,scaleX:1,scaleY:1,x:209.5},0).wait(35));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(207,332,415,736);
// library properties:
lib.properties = {
	id: 'C7460226EE5C4A41A91D0FC75828EA79',
	width: 414,
	height: 664,
	fps: 18,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"../assets/img/illu_CSE200_black_CSE200.png?1519141518423", id:"illu_CSE200_black_CSE200"},
		{src:"../assets/img/Illu_TV_1Device_Mobile.png?1519141518423", id:"Illu_TV_1Device_Mobile"},
		{src:"../assets/img/Illu_TV_2Devices_Mobile.png?1519141518423", id:"Illu_TV_2Devices_Mobile"},
		{src:"../assets/img/Illu_TV_4Devices_Mobile.png?1519141518423", id:"Illu_TV_4Devices_Mobile"},
		{src:"../assets/img/Illu_TV_Black_Mobile.png?1519141518423", id:"Illu_TV_Black_Mobile"},
		{src:"../assets/img/LLaptopon.png?1519141518423", id:"LLaptopon"},
		{src:"../assets/img/LPhoneOn.png?1519141518423", id:"LPhoneon"},
		{src:"../assets/img/LaptopLoff.png?1519141518423", id:"LaptopLoff"},
		{src:"../assets/img/LaptopRoff.png?1519141518423", id:"LaptopRoff"},
		{src:"../assets/img/LockIcon.png?1519141518423", id:"Lockicon"},
		{src:"../assets/img/RLaptopon.png?1519141518423", id:"RLaptopon"},
		{src:"../assets/img/RTabletOn.png?1519141518423", id:"RTableton"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['C7460226EE5C4A41A91D0FC75828EA79'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;