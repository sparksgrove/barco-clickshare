var qs = (function(a) {
if (a == "") return {};
var b = {};
for (var i = 0; i < a.length; ++i) {
    var p = a[i].split('=', 2);
    if (p.length == 1)
        b[p[0]] = "";
    else
        b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
}
return b;
})(window.location.search.substr(1).split('&'));

if(!checkIfFormWasSubmitted()){ // Don't show the form again after it was submitted.
    MktoForms2.loadForm("//app-e.marketo.com", "141-TQB-685", 3059);
} else {
    $(".get-a-quote-success").show();
    $(".form-instruct").hide();
}

     if ($(window).width() < 1024) {

          $('div.col-8').removeClass('col-8');
          $('.logo-corner').attr('src', 'assets/img/mobile/barco-logo-mobile.png');
          $('.main .btn-hdr').attr('href', 'select-device.html');

      }

      checkIfFormWasSubmitted();

      if (qs['unit']) {
          $('h1 span.unitName').text('for ' + qs['unit'] + ':');
          $('.unitTeaser .' + qs['unit']).show();
      }
      else {
        $('.unitTeaser').css('display', 'none');
        $('.col-8').removeClass('col-8');
        $('.half').addClass('no-unitTeaser');
      }

      $('.btn-submit').click(function(e) {
          e.preventDefault();
          $('.container-fluid .get-form, .container-fluid .unitTeaser').hide('');
          $('.container-fluid .success').show();
      });

      if ($(window).width() < 1025) {
          $('div.col-8').removeClass('col-8');
          $('div.col-9').removeClass('col-9');
          $('div.unitTeaser').hide();
          $('.container-fluid .white').removeClass('white');
          $('.logo-corner').attr('src', 'assets/img/mobile/barco-logo-mobile.png');
      }

MktoForms2.whenReady(function (form) {
    var unitQsValue = qs['unit'];

    if(!unitQsValue) {
        $('input[name="websiteProductID"]').val("");
        return;
    }

    var productId = getProductId(unitQsValue)

    $("input[name='websiteProductID']").val(productId);
});

function getProductId(unitName) {
    switch(unitName) {
        case "CS-100":
        return "33A23002-6AFB-45B8-BE26-F2A481CDB811";
        case "CSE-200":
        return "4E1D441F-AC1B-4129-A0D9-5BD62CBB44DD";
        case "CSE-800":
        return "1ADCE9FC-69C4-4120-B79A-4403A965E74A";
    default:
        return "";
    }
}

function checkIfFormWasSubmitted() {
    return qs['aliId']; // Temporary check: aliId is added by Marketo after a succesful form submit.
}