(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.finger_PNG6280 = function() {
	this.initialize(img.finger_PNG6280);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,700,534);


(lib.Img_Share_On_1280x800 = function() {
	this.initialize(img.Img_Share_On_1280x800);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1280,800);


(lib.IntroBackground = function() {
	this.initialize(img.IntroBackground);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1280,800);


(lib.starterpage_clicksarecopy = function() {
	this.initialize(img.starterpage_clicksarecopy);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,228,531);


(lib.whiteGlow_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(255,255,255,0.498)").ss(13,1,1).p("AJVAAQAADuivCnQivCoj3AAQj2AAivioQivinAAjuQAAjtCvioQCvinD2AAQD3AACvCnQCvCoAADtg");
	this.shape.setTransform(59.7,57.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6.5,-6.5,132.4,127.7);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Img_Share_On_1280x800();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,1280,800);


(lib.redGlow = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(240,0,0,0.6)").ss(13,1,1).p("AJVAAQAADuivCnQivCoj3AAQj2AAivioQivinAAjuQAAjtCvioQCvinD2AAQD3AACvCnQCvCoAADtg");
	this.shape.setTransform(59.7,57.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6.5,-6.5,132.4,127.7);


(lib.h_btn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F5F5F5").s().p("AndHdQjFjGAAkXQAAkXDFjGQDGjFEXAAQEXAADGDFQDGDGAAEXQAAEXjGDGQjGDGkXAAQkXAAjGjGg");
	this.shape.setTransform(67.5,67.5);
	this.shape._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(3).to({_off:false},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.finger = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.finger_PNG6280();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,700,534);


(lib.device_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{white_glow:4,yellow_glow:15,red_glow:24});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_10 = function() {
		this.stop();
	}
	this.frame_21 = function() {
		this.stop();
	}
	this.frame_30 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(10).call(this.frame_10).wait(11).call(this.frame_21).wait(9).call(this.frame_30).wait(18));

	// label
	this.instance = new lib.whiteGlow_mc();
	this.instance.parent = this;
	this.instance.setTransform(126.1,406,1,1,0,0,0,59.6,57.3);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({_off:false},0).to({alpha:1},6).wait(38));

	// Layer_1
	this.instance_1 = new lib.starterpage_clicksarecopy();
	this.instance_1.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(48));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,228,531);


// stage content:
(lib.intro_desktop = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.button_product.addEventListener("click", fl_ClickToGoToWebPage);
		
		function fl_ClickToGoToWebPage() {
			window.open("/share-instantly.html", "_self");
		}
	}
	this.frame_20 = function() {
		this.button_product.addEventListener("click", fl_ClickToGoToWebPage);
		
		function fl_ClickToGoToWebPage() {
			window.open("/share-instantly.html", "_self");
		}
	}
	this.frame_26 = function() {
		this.button_product.addEventListener("click", fl_ClickToGoToWebPage);
		
		function fl_ClickToGoToWebPage() {
			window.open("/share-instantly.html", "_self");
		}
	}
	this.frame_28 = function() {
		//this.device_mc.gotoAndPlay('white_glow');
	}
	this.frame_33 = function() {
		this.gotoAndPlay("yellow_glow");
	}
	this.frame_75 = function() {
		//this.device_mc.gotoAndPlay("red_glow");
	}
	this.frame_100 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(20).call(this.frame_20).wait(6).call(this.frame_26).wait(2).call(this.frame_28).wait(5).call(this.frame_33).wait(42).call(this.frame_75).wait(25).call(this.frame_100).wait(1));

	// CTA
	this.button_product = new lib.h_btn();
	this.button_product.name = "button_product";
	this.button_product.parent = this;
	this.button_product.setTransform(669.5,722.1,1,1,0,0,0,67.5,67.5);
	new cjs.ButtonHelper(this.button_product, 0, 1, 2, false, new lib.h_btn(), 3);

	this.timeline.addTween(cjs.Tween.get(this.button_product).wait(20).to({y:494.1},6).to({_off:true},62).wait(13));

	// finger
	this.instance = new lib.finger("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(496,868,1,1,0,0,0,350,267);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(61).to({_off:false},0).to({regX:349.9,regY:267.1,x:536,y:550},7).wait(3).to({regX:350,scaleX:0.99,scaleY:0.99,x:538,y:553.2},0).wait(3).to({regX:349.9,scaleX:1,scaleY:1,x:536,y:550},0).to({x:586,y:870},6).to({_off:true},1).wait(20));

	// Layer_1
	this.instance_1 = new lib.Symbol1("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(640,400,1,1,0,0,0,640,400);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(87).to({_off:false},0).to({alpha:1},5).wait(9));

	// mask (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("EgY1A6BMAAAh0BMAxrABGMAAABy7g");
	mask.setTransform(660.1,495.9);

	// mask
	this.instance_2 = new lib.redGlow("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(670.5,495.1,1,1,0,0,0,59.6,57.3);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	var maskedShapeInstanceList = [this.instance_2];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(71).to({_off:false},0).to({alpha:1},5).wait(25));

	// glow
	this.instance_3 = new lib.whiteGlow_mc("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(670.5,495.1,1,1,0,0,0,59.6,57.3);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	var maskedShapeInstanceList = [this.instance_3];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(28).to({_off:false},0).to({alpha:1},5).wait(68));

	// device
	this.instance_4 = new lib.device_mc();
	this.instance_4.parent = this;
	this.instance_4.setTransform(658,584.5,1,1,0,0,0,114,266.5);

	var maskedShapeInstanceList = [this.instance_4];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(20).to({y:355.5},6).wait(75));

	// Gradient
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["rgba(0,0,0,0)","rgba(0,0,0,0.4)"],[0,1],-40.1,-93.8,-40.1,103.6).s().p("EhkLAPeIAA+7MDIXAAAIAAe7g");
	this.shape.setTransform(638.8,701);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(101));

	// background
	this.instance_5 = new lib.IntroBackground();
	this.instance_5.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(101));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(637.7,400,1282.4,849);
// library properties:
lib.properties = {
	id: 'E18D9131730941C7A1B3DB1635A56CDA',
	width: 1280,
	height: 800,
	fps: 18,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"/assets/img/finger_PNG6280.png?1520023497746", id:"finger_PNG6280"},
		{src:"/assets/img/Img_Share_On_1280x800.jpg?1520023497746", id:"Img_Share_On_1280x800"},
		{src:"/assets/img/IntroBackground.jpg?1520023497746", id:"IntroBackground"},
		{src:"/assets/img/starterpage_clicksarecopy.png?1520023497746", id:"starterpage_clicksarecopy"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['E18D9131730941C7A1B3DB1635A56CDA'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;