      var unit;

      var qs = (function(a) {
          if (a == "") return {};
          var b = {};
          for (var i = 0; i < a.length; ++i) {
              var p = a[i].split('=', 2);
              if (p.length == 1)
                  b[p[0]] = "";
              else
                  b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
          }
          return b;
      })(window.location.search.substr(1).split('&'));

      if (qs['unit']) {
          unit = qs['unit'];
          console.log(unit);
      } else {
          unit = 0;
      }

      $(document).ready(function() {
          $('.carousel').slick({
              //setting-name: setting-value
              arrows: false,
              initialSlide: unit,
              swipeToSlide: true
          });
      });

      if ($(window).width() < 1240) {
          $('div.col-9').removeClass('col-9');
          $('div.unitTeaser').hide();
          $('.container-fluid .white').removeClass('white');
          $('.logo-corner').attr('src', 'assets/img/mobile/barco-logo-mobile.png');
      }

      var qs = (function(a) {
          if (a == "") return {};
          var b = {};
          for (var i = 0; i < a.length; ++i) {
              var p = a[i].split('=', 2);
              if (p.length == 1)
                  b[p[0]] = "";
              else
                  b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
          }
          return b;
      })(window.location.search.substr(1).split('&'));

      if (qs['unit']) {
          $('h1 span.unitName').text(' for ' + qs['unit']);
          $('.unitTeaser .' + qs['unit']).show();
      }

      $('.btn-submit').click(function(e) {
          //e.preventDefault();
          $('.container-fluid .white .col-9, .container-fluid .white .col-3').hide('');
          $('.container-fluid .white .success').show();
      });