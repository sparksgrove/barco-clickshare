var anim_script = document.createElement('script');
//console.log($(window).width());
if ($(window).width() <= 980) {
    anim_script.setAttribute('src', '/assets/js/intro_mobile_.js');
    console.log('mobile anim');
} else {
    anim_script.setAttribute('src', '/assets/js/intro_desktop_.js?1519141669387')
    console.log('desktop anim');
}
document.head.appendChild(anim_script);
var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;

function init() {
    canvas = document.getElementById("canvas");
    anim_container = document.getElementById("animation_container");
    dom_overlay_container = document.getElementById("dom_overlay_container");
    var comp = AdobeAn.getComposition("E18D9131730941C7A1B3DB1635A56CDA");
    var lib = comp.getLibrary();
    var loader = new createjs.LoadQueue(false);
    loader.addEventListener("fileload", function(evt) {
        handleFileLoad(evt, comp)
    });
    loader.addEventListener("complete", function(evt) {
        handleComplete(evt, comp)
    });
    var lib = comp.getLibrary();
    loader.loadManifest(lib.properties.manifest);
    /*
    if ($(window).width() <= 980) {
        setTimeout(function() {
            window.location.href = 'clickshare.html';
        }, 8500);
    } else {
        setTimeout(function() {
            window.location.href = 'share-instantly.html';
        }, 8500);
    }
    */

        setTimeout(function() {
            window.location.href = 'share-instantly.html';
        }, 8500);    
    // Text for animation

    $('#dom_overlay_container').append('<div class="connect">Connect.</div>');
    $('#dom_overlay_container .connect').show();
    setTimeout(function() {
        $('#dom_overlay_container .connect').fadeOut();
    }, 1000);

    setTimeout(function() {
        $('#dom_overlay_container').append('<div class="click">Click.</div>');
        $('#dom_overlay_container .click').fadeIn();
        setTimeout(function() {
            $('#dom_overlay_container .click').fadeOut();
        }, 1800);
    }, 1500);
    setTimeout(function() {
        $('#dom_overlay_container').append('<div class="share">Share.</div>');
        $('#dom_overlay_container .share').fadeIn();
    }, 5500);

}

function handleFileLoad(evt, comp) {
    var images = comp.getImages();
    if (evt && (evt.item.type == "image")) {
        images[evt.item.id] = evt.result;
    }
}

function handleComplete(evt, comp) {
    //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
    var lib = comp.getLibrary();
    var ss = comp.getSpriteSheet();
    var queue = evt.target;
    var ssMetadata = lib.ssMetadata;
    for (i = 0; i < ssMetadata.length; i++) {
        ss[ssMetadata[i].name] = new createjs.SpriteSheet({
            "images": [queue.getResult(ssMetadata[i].name)],
            "frames": ssMetadata[i].frames
        })
    }
    //exportRoot = new lib.intro_desktop();
    if ($(window).width() <= 981) {
        exportRoot = new lib.intro_mobile_();
    } else {
        exportRoot = new lib.intro_desktop();
    }
    stage = new lib.Stage(canvas);
    //Registers the "tick" event listener.
    fnStartAnimation = function() {
        stage.addChild(exportRoot);
        createjs.Ticker.setFPS(lib.properties.fps);
        createjs.Ticker.addEventListener("tick", stage);
    }
    //Code to support hidpi screens and responsive scaling.
    function makeResponsive(isResp, respDim, isScale, scaleType) {
        var lastW, lastH, lastS = 1;
        window.addEventListener('resize', resizeCanvas);
        resizeCanvas();

        function resizeCanvas() {
            //console.log('resize canvas');	
            var w = lib.properties.width,
                h = lib.properties.height;
            var iw = window.innerWidth,
                ih = window.innerHeight;
            var pRatio = window.devicePixelRatio || 1,
                xRatio = iw / w,
                yRatio = ih / h,
                sRatio = 1;
            if (isResp) {
                if ((respDim == 'width' && lastW == iw) || (respDim == 'height' && lastH == ih)) {
                    sRatio = lastS;
                } else if (!isScale) {
                    if (iw < w || ih < h)
                        sRatio = Math.min(xRatio, yRatio);
                } else if (scaleType == 1) {
                    sRatio = Math.min(xRatio, yRatio);
                } else if (scaleType == 2) {
                    sRatio = Math.max(xRatio, yRatio);
                }
            }
            canvas.width = w * pRatio * sRatio;
            canvas.height = h * pRatio * sRatio;
            canvas.style.width = dom_overlay_container.style.width = anim_container.style.width = w * sRatio + 'px';
            canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h * sRatio + 'px';
            stage.scaleX = pRatio * sRatio;
            stage.scaleY = pRatio * sRatio;
            lastW = iw;
            lastH = ih;
            lastS = sRatio;
            stage.tickOnUpdate = false;
            stage.update();
            stage.tickOnUpdate = true;
        }
    }
    makeResponsive(true, 'both', true, 1);
    AdobeAn.compositionLoaded(lib.properties.id);
    fnStartAnimation();
}