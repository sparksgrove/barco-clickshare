(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [
		{name:"Barco_mobile_intro_atlas_", frames: [[0,0,700,534],[416,536,414,736],[0,536,414,736],[702,0,228,533]]}
];


// symbols:



(lib.finger_PNG6280 = function() {
	this.spriteSheet = ss["Barco_mobile_intro_atlas_"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.intro_bg = function() {
	this.spriteSheet = ss["Barco_mobile_intro_atlas_"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.share_bg = function() {
	this.spriteSheet = ss["Barco_mobile_intro_atlas_"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.starterpage_clicksarecopy = function() {
	this.spriteSheet = ss["Barco_mobile_intro_atlas_"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.whiteGlow_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(255,255,255,0.498)").ss(13,1,1).p("AJVAAQAADuivCnQivCoj3AAQj2AAivioQivinAAjuQAAjtCvioQCvinD2AAQD3AACvCnQCvCoAADtg");
	this.shape.setTransform(59.7,57.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6.5,-6.5,132.4,127.7);


(lib.txt_share = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgpAqQgSgRAAgZQAAgZARgRQARgRAZAAQAZAAASARQARARAAAZQAAAZgRARQgSASgZAAQgYAAgRgSg");
	this.shape.setTransform(217,58.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AhxCLQgugzAAhYQAAhXArgzQAsgzBNAAQBOAAAnAyQAmAzAABeIjaAfQALBQBMAAQAdAAAZgFQAZgFAQgJIAYBCQgXALghAIQghAHgqAAQhTAAgvgzgAhBgOIB/gTQAAgwgQgXQgPgXgeAAQhCAAAABxg");
	this.shape_1.setTransform(188.8,46);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AhhC6IAAlZQAegPAtgFQArgGBNAAIgTBKQgVgIgUABQgSgBgMAHIAAEqg");
	this.shape_2.setTransform(159.5,45.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AiXBKQAAhBA3gcQA2gdBfgJIAAgRQAAgbgRgLQgQgLgfAAQgZAAgYAGQgYAGgWAKIgXg8QAfgOAngHQAmgHAdAAQBEAAAmAfQAmAfAABIIAADaQgYANgkAHQglAHgvAAQifAAAAh0gAgaANQgZAQAAApQgBAgASAPQAQAOAgAAQAYAAAPgHIAAiEQg1AFgaAQg");
	this.shape_3.setTransform(126.5,46);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAyELIAAj/QAAgWgNgOQgNgOgcAAQgZAAgUAKIAAEnIhqAAIAAoVIBqAAIAACzQAngQAnAAQBBABAfAgQAfAfAAAyIAAEAg");
	this.shape_4.setTransform(89.4,37.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AhZDqQgcgGgWgPIAZhNQAPAKATAGQASAGAYAAQAdAAAQgPQARgPAAgaQAAgXgPgPQgNgPgrgTQgrgUgWghQgWgiAAgmQAAg+ApgpQApgpBBgBQAmAAAaAHQAaAHAUAOIgXBDQgPgJgRgGQgRgFgVAAQg4AAAAA2QAAAXAPAPQAOAPAwAWQAwAVAUAgQAVAiAAArQAABAgsApQgsAqhEAAQgsAAgdgHg");
	this.shape_5.setTransform(53,40.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.txt_share, new cjs.Rectangle(34.8,0,193.9,83.4), null);


(lib.txt_connect = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgpAqQgSgRAAgZQAAgZARgRQARgRAZAAQAZAAASARQARARAAAZQAAAZgRARQgSASgZAAQgYAAgRgSg");
	this.shape.setTransform(251.8,58.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AghDQQgbgeAAguIAAjEIgoAAIAAhEIAoAAIAAhPIBpgbIAABqIA4AAIAABEIg4AAIAAC8QAAArAiAAIAWgEIAABCIgWAGIgeADQg2AAgcgeg");
	this.shape_1.setTransform(229.9,41.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AhOCMQgpgzAAhZQAAhZAqgyQArgyA/AAQAfAAAXAHQAWAIAOAMIgWA6IgSgLQgJgDgQAAQgiAAgRAeQgQAeAAA5QAAA2ARAfQARAfAgAAQAcAAAQgPIAXBAQgNAMgWAHQgVAHgiAAQhCAAgqgyg");
	this.shape_2.setTransform(203.7,46);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AhxCLQgugzAAhYQAAhXArgzQAsgzBNAAQBOAAAnAyQAmAzAABeIjaAfQALBQBMAAQAdAAAZgFQAZgFAQgJIAYBCQgXALghAIQghAHgqAAQhTAAgvgzgAhBgOIB/gTQAAgwgQgXQgPgXgeAAQhCAAAABxg");
	this.shape_3.setTransform(170.3,46);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAyC6IAAj/QAAgXgNgOQgMgOgdAAIgXACIgWAGIAAEqIhqAAIAAlYQAdgNAngIQAmgGAwAAQBSAAAlAeQAmAfAAA2IAAEAg");
	this.shape_4.setTransform(132,45.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AAyC6IAAj/QAAgXgNgOQgMgOgdAAIgXACIgWAGIAAEqIhqAAIAAlYQAdgNAngIQAmgGAwAAQBSAAAlAeQAmAfAAA2IAAEAg");
	this.shape_5.setTransform(92.8,45.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("Ah3CJQgqg0AAhVQAAhUAqg1QAqg0BNAAQBNAAArA0QAqA1AABUQAABVgqA0QgqA1hOAAQhNAAgqg1gAgqhaQgNAhAAA4QAAA3ANAgQALAgAfAAQAgAAAMggQAMgfAAg4QAAg5gMghQgMggggAAQgfAAgLAhg");
	this.shape_6.setTransform(54.4,46);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AhfCsQgwhGABhmQAAhkAvhGQAwhGBYAAQBBAAAkAcIgVBCIgYgKQgNgEgTAAQgyAAgVAuQgXAtAABFQAABCAYAtQAWAuAwAAIAdgDIAZgLIAZBGQglAchDAAQhYAAgvhFg");
	this.shape_7.setTransform(19.5,40.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.txt_connect, new cjs.Rectangle(0,0,263.5,83.4), null);


(lib.txt_click = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgpAqQgSgRAAgZQAAgZARgRQARgRAZAAQAZAAASARQARARAAAZQAAAZgRARQgSASgZAAQgYAAgRgSg");
	this.shape.setTransform(200.2,58.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAvELIhoi5IgBC5IhqAAIAAoVIBqAAIAAFJIBmihIBqAAIhzCjICDDKg");
	this.shape_1.setTransform(174.3,37.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AhOCMQgpgzAAhZQAAhZAqgyQArgyA/AAQAfAAAXAHQAWAIAOAMIgWA6IgSgLQgJgDgQAAQgiAAgRAeQgQAeAAA5QAAA2ARAfQARAfAgAAQAcAAAQgPIAXBAQgNAMgWAHQgVAHgiAAQhCAAgqgyg");
	this.shape_2.setTransform(139.7,46);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("Ag0EFIAAluIBpAAIAAFugAgnivQgQgOAAgVQAAgVAQgOQARgPAWAAQAYAAAQAOQAQAPAAAVQAAAVgQAOQgQAOgYAAQgXAAgQgOg");
	this.shape_3.setTransform(115.5,38.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("Ag0ELIAAoVIBpAAIAAIVg");
	this.shape_4.setTransform(96.5,37.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AhfCsQgwhGAAhmQABhkAvhGQAvhGBaAAQBAAAAkAcIgVBCIgYgKQgNgEgTAAQgyAAgVAuQgXAtAABFQAABCAYAtQAWAuAwAAIAdgDIAZgLIAYBGQgjAchDAAQhZAAgvhFg");
	this.shape_5.setTransform(71,40.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.txt_click, new cjs.Rectangle(51.6,0,160.3,83.4), null);


(lib.share_bg_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.share_bg();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.share_bg_mc, new cjs.Rectangle(0,0,414,736), null);


(lib.redGlow = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(240,0,0,0.498)").ss(13,1,1).p("AJVAAQAADuivCnQivCoj3AAQj2AAivioQivinAAjuQAAjtCvioQCvinD2AAQD3AACvCnQCvCoAADtg");
	this.shape.setTransform(59.7,57.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6.5,-6.5,132.4,127.7);


(lib.finger = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.finger_PNG6280();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,700,534);


(lib.device_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{white_glow:4,yellow_glow:15,red_glow:24});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_10 = function() {
		this.stop();
	}
	this.frame_21 = function() {
		this.stop();
	}
	this.frame_30 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(10).call(this.frame_10).wait(11).call(this.frame_21).wait(9).call(this.frame_30).wait(18));

	// label
	this.instance = new lib.whiteGlow_mc();
	this.instance.parent = this;
	this.instance.setTransform(126.1,406,1,1,0,0,0,59.6,57.3);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({_off:false},0).to({alpha:1},6).wait(38));

	// Layer_1
	this.instance_1 = new lib.starterpage_clicksarecopy();
	this.instance_1.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(48));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,228,533);


// stage content:
(lib.Barco_mobile_intro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_28 = function() {
		this.device_mc.gotoAndPlay('white_glow');
	}
	this.frame_33 = function() {
		this.gotoAndPlay("yellow_glow");
	}
	this.frame_68 = function() {
		//this.device_mc.gotoAndPlay("red_glow");
	}
	this.frame_98 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(28).call(this.frame_28).wait(5).call(this.frame_33).wait(35).call(this.frame_68).wait(30).call(this.frame_98).wait(1));

	// Layer_3
	this.instance = new lib.finger("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(28,808,1,1,0,0,0,350,267);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(39).to({_off:false},0).to({regX:349.9,regY:267.1,x:68,y:550},7).wait(3).to({regX:350,scaleX:0.99,scaleY:0.99,x:70,y:553.2},0).wait(3).to({regX:349.9,scaleX:1,scaleY:1,x:68,y:550},0).to({x:98,y:810},6).to({_off:true},1).wait(40));

	// Text
	this.instance_1 = new lib.txt_connect();
	this.instance_1.parent = this;
	this.instance_1.setTransform(202.8,271.6,1,1,0,0,0,131.8,41.6);

	this.instance_2 = new lib.txt_click();
	this.instance_2.parent = this;
	this.instance_2.setTransform(202.8,662.3,1,1,0,0,0,131.8,41.6);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.instance_3 = new lib.txt_share();
	this.instance_3.parent = this;
	this.instance_3.setTransform(282.8,572.3,1,1,0,0,0,131.8,41.6);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(15).to({alpha:0},5).to({_off:true},39).wait(40));
	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(59).to({_off:false},0).to({alpha:1},5).wait(21).to({alpha:0},5).to({_off:true},3).wait(6));
	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(93).to({_off:false},0).to({alpha:1},5).wait(1));

	// Gradient
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["rgba(0,0,0,0)","rgba(0,0,0,0.6)"],[0,1],0.1,-63.8,0.1,63.9).s().p("EggVAKAIAAz/MBArAAAIAAT/g");
	this.shape.setTransform(207,672);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(99));

	// share_bg
	this.instance_4 = new lib.share_bg_mc();
	this.instance_4.parent = this;
	this.instance_4.setTransform(207,368,1,1,0,0,0,207,368);
	this.instance_4.alpha = 0;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(85).to({_off:false},0).to({alpha:1},5).wait(9));

	// mask (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("EgY1AunMAAAhdNMAxrABGMAAABcHg");
	mask.setTransform(190.1,436.9);

	// Layer_2
	this.instance_5 = new lib.redGlow("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(200.5,508.1,1,1,0,0,0,59.6,57.3);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	var maskedShapeInstanceList = [this.instance_5];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(49).to({_off:false},0).to({alpha:1},5).wait(45));

	// glow
	this.instance_6 = new lib.whiteGlow_mc("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(200.5,508.1,1,1,0,0,0,59.6,57.3);
	this.instance_6.alpha = 0;
	this.instance_6._off = true;

	var maskedShapeInstanceList = [this.instance_6];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(28).to({_off:false},0).to({alpha:1},5).to({_off:true},16).wait(50));

	// device
	this.instance_7 = new lib.device_mc();
	this.instance_7.parent = this;
	this.instance_7.setTransform(188,597.5,1,1,0,0,0,114,266.5);

	this.device_mc = new lib.device_mc();
	this.device_mc.name = "device_mc";
	this.device_mc.parent = this;
	this.device_mc.setTransform(188,368.5,1,1,0,0,0,114,266.5);

	var maskedShapeInstanceList = [this.instance_7,this.device_mc];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_7}]}).to({state:[{t:this.instance_7}]},20).to({state:[{t:this.device_mc}]},6).wait(73));
	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(20).to({_off:true,y:368.5},6).wait(73));

	// background
	this.instance_8 = new lib.intro_bg();
	this.instance_8.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(99));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(207,368,414,736);
// library properties:
lib.properties = {
	id: 'E18D9131730941C7A1B3DB1635A56CDA',
	width: 414,
	height: 736,
	fps: 18,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"../assets/img/Barco_mobile_intro_atlas_.png?1515011375017", id:"Barco_mobile_intro_atlas_"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['E18D9131730941C7A1B3DB1635A56CDA'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;