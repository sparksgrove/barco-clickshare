(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.Desktop_LPhoneOn = function() {
	this.initialize(img.Desktop_LPhoneOn);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,139,64);


(lib.Desktop_RTabletOn = function() {
	this.initialize(img.Desktop_RTabletOn);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,141,106);


(lib.illu_CSE200_black_CSE200 = function() {
	this.initialize(img.illu_CSE200_black_CSE200);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,106,76);


(lib.LLaptop_Off_Desktop = function() {
	this.initialize(img.LLaptop_Off_Desktop);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,323,172);


(lib.LLaptop_On_Desktop = function() {
	this.initialize(img.LLaptop_On_Desktop);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,320,166);


(lib.LockIcon = function() {
	this.initialize(img.LockIcon);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,34,37);


(lib.RLaptop_Off_Desktop = function() {
	this.initialize(img.RLaptop_Off_Desktop);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,325,168);


(lib.RLaptop_On_Desktop = function() {
	this.initialize(img.RLaptop_On_Desktop);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,325,168);


(lib.TV1Device = function() {
	this.initialize(img.TV1Device);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,398,239);


(lib.TV2Devices = function() {
	this.initialize(img.TV2Devices);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,398,240);


(lib.TV4Devices = function() {
	this.initialize(img.TV4Devices);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,398,240);


(lib.TVOff = function() {
	this.initialize(img.TVOff);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,398,239);


(lib.Symbol12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AgfA9QgRgZAAgkQAAgjARgZQARgYAdAAQALAAAJADQAHABAHAGIgHASIgJgFQgGgBgGAAQgUAAgIASQgJASAAAaQAAAbAKARQAIASATAAIALgBIAJgFIAIATQgMAKgXAAQgdAAgQgYg");
	mask.setTransform(92.9,23.4);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#D70000","#F00000"],[0,1],0,22.5,0,-22.4).s().p("AwgDcQgrAAAAgnIAAlpQAAgnArAAMAhAAAAQAsAAAAAnIAAFpQAAAngsAAg");
	this.shape.setTransform(110,22);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,220,44);


(lib.Symbol11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.LockIcon();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,34,37);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["rgba(144,186,227,0.149)","rgba(144,186,227,0.6)"],[0.416,1],0,0,0,0,0,62.5).s().p("AmyGzQi0i0AAj/QAAj+C0i0QC0i0D+AAQD/AAC0C0QC0C0AAD+QAAD/i0C0Qi0C0j/AAQj+AAi0i0g");
	this.shape.setTransform(61.5,61.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,123,123);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.TV4Devices();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,398,240);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Desktop_LPhoneOn();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,139,64);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Desktop_RTabletOn();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,141,106);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.TV2Devices();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,398,240);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.RLaptop_On_Desktop();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,325,168);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.RLaptop_Off_Desktop();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,325,168);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.TV1Device();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,398,239);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.TVOff();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,398,239);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.LLaptop_On_Desktop();
	this.instance.parent = this;
	this.instance.setTransform(-1,1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1,1,320,166);


// stage content:
(lib.share_desktop_700x700 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_77 = function() {
		/* Click to Go to Web Page
		Clicking on the specified symbol instance loads the URL in a new browser window.
		
		Instructions:
		1. Replace http://www.adobe.com with the desired URL address.
		   Keep the quotation marks ("").
		*/
	}
	this.frame_261 = function() {
		this.button_1.addEventListener("click", fl_ClickToGoToWebPage);
		
		function fl_ClickToGoToWebPage() {
			window.open("http://barco.demo.sparksgrove.com/save-space.html", "_self");
		}
	}
	this.frame_271 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(77).call(this.frame_77).wait(184).call(this.frame_261).wait(10).call(this.frame_271).wait(1));

	// mask (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_172 = new cjs.Graphics().p("AdldDIARjlIaLB+IgSDlg");
	var mask_graphics_173 = new cjs.Graphics().p("Ablc4IASjlIaKB+IgRDmg");
	var mask_graphics_174 = new cjs.Graphics().p("AZmctIARjlIaLB+IgSDmg");
	var mask_graphics_175 = new cjs.Graphics().p("AXmcjIASjmIaKB/IgRDlg");
	var mask_graphics_176 = new cjs.Graphics().p("AVncYIARjmIaLB/IgSDlg");
	var mask_graphics_177 = new cjs.Graphics().p("ATncNIASjlIaKB+IgRDlg");
	var mask_graphics_178 = new cjs.Graphics().p("ARocCIARjlIaLB+IgSDlg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(172).to({graphics:mask_graphics_172,x:358.5,y:198.5}).wait(1).to({graphics:mask_graphics_173,x:345.7,y:197.5}).wait(1).to({graphics:mask_graphics_174,x:333,y:196.4}).wait(1).to({graphics:mask_graphics_175,x:320.2,y:195.3}).wait(1).to({graphics:mask_graphics_176,x:307.5,y:194.2}).wait(1).to({graphics:mask_graphics_177,x:294.7,y:193.1}).wait(1).to({graphics:mask_graphics_178,x:282,y:192}).wait(94));

	// line 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.498)").s().p("AJ0BPQgIgJAAgMQAAgNAIgJQAJgIANAAQAMAAAJAIQAJAJAAANQAAAMgJAJQgJAJgMAAQgNAAgJgJgAIQBGQgIgJAAgMQAAgNAIgJQAJgIANAAQAMAAAJAIQAJAJAAANQAAAMgJAJQgJAJgMAAQgNAAgJgJgAGsA+QgIgJAAgMQAAgNAIgJQAJgIANAAQAMAAAJAIQAJAJAAANQAAAMgJAJQgJAJgMAAQgNAAgJgJgAFJA1QgJgJAAgMQAAgNAJgJQAJgIAMAAQANAAAIAIQAJAJAAANQAAAMgJAJQgIAJgNAAQgMAAgJgJgADlAsQgJgIAAgNQAAgMAJgJQAJgIAMAAQANAAAIAIQAJAJAAAMQAAANgJAIQgIAJgNAAQgMAAgJgJgACBAjQgJgIAAgNQAAgMAJgIQAJgJAMAAQANAAAIAJQAJAIAAAMQAAANgJAIQgIAJgNAAQgMAAgJgJgAAdAaQgJgIAAgNQAAgLAJgJQAJgJAMAAQANAAAIAJQAJAJAAALQAAANgJAIQgIAJgNAAQgMAAgJgJgAhGARQgJgIAAgMQAAgMAJgJQAJgJAMAAQANAAAIAJQAJAJAAAMQAAAMgJAIQgIAJgNAAQgMAAgJgJgAiqAIQgJgIAAgMQAAgMAJgJQAJgJAMAAQANAAAIAJQAJAJAAAMQAAAMgJAIQgIAJgNAAQgMAAgJgJgAkOAAQgJgIAAgNQAAgMAJgJQAJgJAMAAQANAAAIAJQAJAJAAAMQAAANgJAIQgIAIgNAAQgMAAgJgIgAlygJQgJgIAAgNQAAgMAJgJQAJgJAMAAQANAAAIAJQAJAJAAAMQAAANgJAIQgIAJgNAAQgMAAgJgJgAnWgSQgJgIAAgNQAAgMAJgJQAJgJAMAAQANAAAIAJQAJAJAAAMQAAANgJAIQgIAJgNAAQgMAAgJgJgAo6gbQgJgIAAgNQAAgMAJgJQAJgJAMAAQANAAAIAJQAJAJAAAMQAAANgJAIQgIAJgNAAQgMAAgJgJgAqegkQgJgIAAgNQAAgMAJgJQAJgJAMAAQANAAAIAJQAJAJAAAMQAAANgJAIQgIAJgNAAQgMAAgJgJg");
	this.shape.setTransform(475,365.7);
	this.shape._off = true;

	var maskedShapeInstanceList = [this.shape];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(172).to({_off:false},0).wait(100));

	// mask (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	var mask_1_graphics_165 = new cjs.Graphics().p("AtNeTIZulIIAtDhI5uFJg");
	var mask_1_graphics_166 = new cjs.Graphics().p("Ar9d+IZtlOIAuDhI5tFPg");
	var mask_1_graphics_167 = new cjs.Graphics().p("AqSdpIZslUIAuDhI5rFVg");
	var mask_1_graphics_168 = new cjs.Graphics().p("AondUIZqlaIAwDhI5qFag");
	var mask_1_graphics_169 = new cjs.Graphics().p("Am8dAIZplhIAwDhI5pFgg");
	var mask_1_graphics_170 = new cjs.Graphics().p("AlRcrIZolmIAxDgI5oFmg");
	var mask_1_graphics_171 = new cjs.Graphics().p("AjmcWIZnlsIAxDhI5mFrg");

	this.timeline.addTween(cjs.Tween.get(mask_1).to({graphics:null,x:0,y:0}).wait(165).to({graphics:mask_1_graphics_165,x:79.3,y:216.5}).wait(1).to({graphics:mask_1_graphics_166,x:92.6,y:214.4}).wait(1).to({graphics:mask_1_graphics_167,x:103.2,y:212.3}).wait(1).to({graphics:mask_1_graphics_168,x:113.9,y:210.1}).wait(1).to({graphics:mask_1_graphics_169,x:124.5,y:208}).wait(1).to({graphics:mask_1_graphics_170,x:135.2,y:205.9}).wait(1).to({graphics:mask_1_graphics_171,x:145.9,y:203.8}).wait(101));

	// line 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(0,0,0,0.498)").s().p("AphCPQgIgIAAgNQAAgMAIgJQAKgJAMAAQANAAAIAJQAJAJAAAMQAAANgJAIQgIAJgNAAQgMAAgKgJgAn/B6QgIgIAAgNQAAgMAIgJQAKgJAMAAQANAAAIAJQAJAJAAAMQAAANgJAIQgIAJgNAAQgMAAgKgJgAmdBmQgIgIAAgNQAAgMAIgJQAKgJAMAAQANAAAIAJQAJAJAAAMQAAANgJAIQgIAJgNAAQgMAAgKgJgAk6BRQgJgIAAgNQAAgMAJgJQAJgJAMAAQAMAAAJAJQAJAJAAAMQAAANgJAIQgJAJgMAAQgMAAgJgJgAjYA9QgJgIAAgNQAAgMAJgJQAIgJANAAQAMAAAJAJQAJAJAAAMQAAANgJAIQgJAJgMAAQgNAAgIgJgAh2AoQgJgIAAgNQAAgMAJgIQAIgJANAAQAMAAAJAJQAJAIAAAMQAAANgJAIQgJAJgMAAQgNAAgIgJgAgUAUQgJgIAAgMQAAgMAJgJQAJgJALAAQAMAAAJAJQAJAJAAAMQAAAMgJAIQgJAJgMAAQgLAAgJgJgABMAAQgIgIAAgNQAAgMAIgJQAJgJANAAQAMAAAJAJQAJAJAAAMQAAANgJAIQgJAIgMAAQgNAAgJgIgACugUQgIgIAAgNQAAgMAIgJQAJgJANAAQAMAAAJAJQAJAJAAAMQAAANgJAIQgJAJgMAAQgNAAgJgJgAEQgoQgIgJAAgMQAAgNAIgJQAKgIAMAAQANAAAIAIQAJAJAAANQAAAMgJAJQgIAJgNAAQgMAAgKgJgAFyg8QgIgJAAgMQAAgNAIgJQAKgIAMAAQANAAAIAIQAJAJAAANQAAAMgJAJQgIAJgNAAQgMAAgKgJgAHUhQQgIgJAAgMQAAgNAIgJQAKgIAMAAQANAAAIAIQAJAJAAANQAAAMgJAJQgIAJgNAAQgMAAgKgJgAI3hkQgJgJAAgMQAAgNAJgJQAJgIAMAAQAMAAAJAIQAJAJAAANQAAAMgJAJQgJAJgMAAQgMAAgJgJg");
	this.shape_1.setTransform(226.2,374.2);
	this.shape_1._off = true;

	var maskedShapeInstanceList = [this.shape_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(165).to({_off:false},0).wait(107));

	// mask (mask)
	var mask_2 = new cjs.Shape();
	mask_2._off = true;
	var mask_2_graphics_96 = new cjs.Graphics().p("AZ6c5IDeg6IGpZZIjeA6g");
	var mask_2_graphics_97 = new cjs.Graphics().p("AZTazIDfg6IGoZZIjfA6g");
	var mask_2_graphics_98 = new cjs.Graphics().p("AYtYtIDfg6IGmZaIjeA6g");
	var mask_2_graphics_99 = new cjs.Graphics().p("AYHWnIDeg6IGmZaIjfA6g");
	var mask_2_graphics_100 = new cjs.Graphics().p("AXgUhIDfg5IGlZaIjfA5g");
	var mask_2_graphics_101 = new cjs.Graphics().p("AW6SbIDfg5IGjZaIjfA6g");
	var mask_2_graphics_102 = new cjs.Graphics().p("AWTQVIDfg5IGiZaIjeA6g");

	this.timeline.addTween(cjs.Tween.get(mask_2).to({graphics:null,x:0,y:0}).wait(96).to({graphics:mask_2_graphics_96,x:230.5,y:347.4}).wait(1).to({graphics:mask_2_graphics_97,x:226.6,y:334}).wait(1).to({graphics:mask_2_graphics_98,x:222.6,y:320.7}).wait(1).to({graphics:mask_2_graphics_99,x:218.7,y:307.3}).wait(1).to({graphics:mask_2_graphics_100,x:214.8,y:293.9}).wait(1).to({graphics:mask_2_graphics_101,x:210.8,y:280.6}).wait(1).to({graphics:mask_2_graphics_102,x:206.8,y:267.2}).wait(170));

	// line 1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(0,0,0,0.498)").s().p("ACiLtQgJgJAAgNQAAgMAJgJQAJgIAMAAQAMAAAJAIQAJAJAAAMQAAANgJAJQgJAIgMABQgMgBgJgIgACKKMQgJgJAAgMQAAgNAJgJQAJgJAMAAQAMAAAJAJQAJAJAAANQAAAMgJAJQgJAJgMAAQgMAAgJgJgABwIqQgIgIAAgMQAAgNAIgJQAKgJAMAAQANAAAIAJQAJAJAAANQAAAMgJAIQgIAKgNgBQgMABgKgKgABYHJQgIgIAAgMQAAgNAIgJQAJgJANABQAMgBAJAJQAJAJAAANQAAAMgJAIQgJAJgMAAQgNAAgJgJgABAFpQgJgJAAgNQAAgMAJgJQAIgIANAAQAMAAAJAIQAJAJAAAMQAAANgJAJQgJAIgMABQgNgBgIgIgAAnEIQgIgJAAgNQAAgMAIgJQAKgJAMAAQANAAAIAJQAJAJAAAMQAAANgJAJQgIAIgNABQgMgBgKgIgAAOCnQgIgJAAgMQAAgNAIgJQAKgJAMAAQANAAAIAJQAJAJAAANQAAAMgJAJQgIAJgNgBQgMABgKgJgAgIBFQgJgIAAgMQAAgNAJgJQAIgJAMABQAMgBAJAJQAJAJAAANQAAAMgJAIQgJAKgMgBQgMABgIgKgAghgbQgJgIAAgNQAAgMAJgJQAIgIANAAQAMAAAIAIQAJAJAAAMQAAANgJAIQgIAJgMAAQgNAAgIgJgAg6h7QgIgJAAgNQAAgMAIgJQAKgIAMgBQANABAIAIQAJAJAAAMQAAANgJAJQgIAIgNABQgMgBgKgIgAhTjcQgIgJAAgMQAAgNAIgJQAJgJANAAQAMAAAJAJQAJAJAAANQAAAMgJAJQgJAJgMAAQgNAAgJgJgAhqk+QgJgIAAgMQAAgNAJgJQAIgJANAAQAMAAAJAJQAJAJAAANQAAAMgJAIQgJAKgMgBQgNABgIgKgAiDmfQgJgIAAgNQAAgMAJgJQAJgJAMABQAMgBAJAJQAJAJAAAMQAAANgJAIQgJAJgMAAQgMAAgJgJgAicn/QgIgJAAgNQAAgMAIgJQAKgIAMAAQANAAAIAIQAJAJAAAMQAAANgJAJQgIAIgNABQgMgBgKgIgAizpgQgJgJAAgNQAAgMAJgJQAJgJAMAAQAMAAAJAJQAJAJAAAMQAAANgJAJQgJAIgMABQgNgBgIgIgAjLrCQgJgIAAgMQAAgNAJgJQAJgJAMAAQAMAAAJAJQAJAJAAANQAAAMgJAIQgJAKgMgBQgMABgJgKg");
	this.shape_2.setTransform(384.3,452.7);
	this.shape_2._off = true;

	var maskedShapeInstanceList = [this.shape_2];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_2;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(96).to({_off:false},0).wait(176));

	// mask (mask)
	var mask_3 = new cjs.Shape();
	mask_3._off = true;
	var mask_3_graphics_32 = new cjs.Graphics().p("EANyA1nIGp5ZIDfA6ImqZZg");
	var mask_3_graphics_33 = new cjs.Graphics().p("EAORAzfIGo5ZIDeA6ImoZZg");
	var mask_3_graphics_34 = new cjs.Graphics().p("EAOwAxXIGm5aIDfA6ImnZag");
	var mask_3_graphics_35 = new cjs.Graphics().p("EAPOAvPIGm5aIDfA6ImmZZg");
	var mask_3_graphics_36 = new cjs.Graphics().p("EAPtAtHIGl5aIDeA5ImkZag");
	var mask_3_graphics_37 = new cjs.Graphics().p("EAQMAq/IGj5bIDfA6ImjZag");
	var mask_3_graphics_38 = new cjs.Graphics().p("EAQrAo2IGi5aIDfA5ImiZbg");

	this.timeline.addTween(cjs.Tween.get(mask_3).to({graphics:null,x:0,y:0}).wait(32).to({graphics:mask_3_graphics_32,x:153,y:348.9}).wait(1).to({graphics:mask_3_graphics_33,x:155.9,y:335.3}).wait(1).to({graphics:mask_3_graphics_34,x:158.9,y:321.7}).wait(1).to({graphics:mask_3_graphics_35,x:161.9,y:308}).wait(1).to({graphics:mask_3_graphics_36,x:164.8,y:294.4}).wait(1).to({graphics:mask_3_graphics_37,x:167.8,y:280.8}).wait(1).to({graphics:mask_3_graphics_38,x:170.8,y:267.2}).wait(234));

	// line 1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("rgba(0,0,0,0.498)").s().p("AjMLtQgIgJAAgNQAAgMAIgJQAKgIAMAAQANAAAIAIQAJAJAAAMQAAANgJAJQgIAIgNABQgMgBgKgIgAizKMQgJgJAAgMQAAgNAJgJQAJgJAMAAQAMAAAJAJQAJAJAAANQAAAMgJAJQgJAJgMAAQgMAAgJgJgAiaIqQgJgIAAgMQAAgNAJgJQAIgJANAAQAMAAAJAJQAJAJAAANQAAAMgJAIQgJAKgMgBQgNABgIgKgAiDHJQgIgIAAgMQAAgNAIgJQAJgJANABQAMgBAJAJQAJAJAAANQAAAMgJAIQgJAJgMAAQgNAAgJgJgAhqFpQgIgJAAgNQAAgMAIgJQAKgIAMAAQANAAAIAIQAJAJAAAMQAAANgJAJQgIAIgNABQgMgBgKgIgAhREIQgJgJAAgNQAAgMAJgJQAIgJANAAQAMAAAJAJQAJAJAAAMQAAANgJAJQgJAIgMABQgNgBgIgIgAg4CnQgJgJAAgMQAAgNAJgJQAJgJAMAAQAMAAAJAJQAJAJAAANQAAAMgJAJQgJAJgMgBQgMABgJgJgAghBFQgIgIAAgMQAAgNAIgJQAKgJAMABQAMgBAIAJQAJAJAAANQAAAMgJAIQgIAKgMgBQgMABgKgKgAgHgbQgJgIAAgNQAAgMAJgJQAIgIAMAAQANAAAIAIQAJAJAAAMQAAANgJAIQgIAJgNAAQgMAAgIgJgAAQh7QgJgJAAgNQAAgMAJgJQAIgIANgBQAMABAJAIQAJAJAAAMQAAANgJAJQgJAIgMABQgNgBgIgIgAAojcQgIgJAAgMQAAgNAIgJQAJgJANAAQAMAAAJAJQAJAJAAANQAAAMgJAJQgJAJgMAAQgNAAgJgJgABAk+QgIgIAAgMQAAgNAIgJQAKgJAMAAQANAAAIAJQAJAJAAANQAAAMgJAIQgIAKgNgBQgMABgKgKgABamfQgJgIAAgNQAAgMAJgJQAJgJAMABQAMgBAJAJQAJAJAAAMQAAANgJAIQgJAJgMAAQgMAAgJgJgAByn/QgJgJAAgNQAAgMAJgJQAJgIAMAAQAMAAAJAIQAJAJAAAMQAAANgJAJQgJAIgMABQgMgBgJgIgACJpgQgIgJAAgNQAAgMAIgJQAJgJANAAQANAAAIAJQAJAJAAAMQAAANgJAJQgIAIgNABQgNgBgJgIgACirCQgJgIAAgMQAAgNAJgJQAJgJAMAAQAMAAAJAJQAJAJAAANQAAAMgJAIQgJAKgMgBQgMABgJgKg");
	this.shape_3.setTransform(312.7,452.7);
	this.shape_3._off = true;

	var maskedShapeInstanceList = [this.shape_3];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_3;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(32).to({_off:false},0).wait(240));

	// mask (mask)
	var mask_4 = new cjs.Shape();
	mask_4._off = true;
	mask_4.graphics.p("A87QkMAAAghHMA53AAAMAAAAhHg");
	mask_4.setTransform(347.3,135.6);

	// big screen C
	this.instance = new lib.Symbol9("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(306.5,113.5,1,1,0,0,0,158.5,96.5);
	this.instance.alpha = 0;
	this.instance._off = true;

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_4;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(181).to({_off:false},0).to({alpha:1},6).wait(85));

	// big screen B
	this.instance_1 = new lib.Symbol6("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(306.5,113.5,1,1,0,0,0,158.5,96.5);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_4;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(105).to({_off:false},0).to({alpha:1},6).wait(161));

	// big screen A
	this.instance_2 = new lib.Symbol3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(306.5,113.5,1,1,0,0,0,158.5,96.5);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	var maskedShapeInstanceList = [this.instance_2];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_4;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(41).to({_off:false},0).to({alpha:1},6).wait(225));

	// big screen
	this.instance_3 = new lib.Symbol2("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(306.5,113.5,1,1,0,0,0,158.5,96.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(272));

	// mask (mask)
	var mask_5 = new cjs.Shape();
	mask_5._off = true;
	mask_5.graphics.p("AtBmgIYmiIIBdPFI4wCMg");
	mask_5.setTransform(112,492.1);

	// Layer_3
	this.instance_4 = new lib.Symbol1("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(108,492,1,1,0,0,0,102,66);
	this.instance_4.alpha = 0;
	this.instance_4._off = true;

	var maskedShapeInstanceList = [this.instance_4];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_5;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(26).to({_off:false},0).to({alpha:1},5).wait(241));

	// left laptop
	this.instance_5 = new lib.LLaptop_Off_Desktop();
	this.instance_5.parent = this;
	this.instance_5.setTransform(6,426);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(272));

	// mask (mask)
	var mask_6 = new cjs.Shape();
	mask_6._off = true;
	mask_6.graphics.p("AlaHQInPgVIAxu7IGeAZISEA9Ig3Org");
	mask_6.setTransform(587.7,485.8);

	// Layer_5
	this.instance_6 = new lib.Symbol5("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(467.5,483.5,1,1,0,0,0,96.5,62.5);
	this.instance_6.alpha = 0;
	this.instance_6._off = true;

	var maskedShapeInstanceList = [this.instance_6];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_6;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(90).to({_off:false},0).to({alpha:1},4).wait(178));

	// right laptop
	this.instance_7 = new lib.Symbol4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(467.5,483.5,1,1,0,0,0,96.5,62.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(272));

	// tablet
	this.instance_8 = new lib.Symbol7("synched",0);
	this.instance_8.parent = this;
	this.instance_8.setTransform(575.5,469.5,1,1,0,0,0,49.5,37.5);
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(160).to({_off:false},0).to({x:595.5,y:351.5},7).wait(105));

	// phone
	this.instance_9 = new lib.Symbol8("synched",0);
	this.instance_9.parent = this;
	this.instance_9.setTransform(68.5,464.5,1,1,0,0,0,43.5,20.5);
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(155).to({_off:false},0).to({x:64.5,y:373.5},7).wait(110));

	// device
	this.instance_10 = new lib.illu_CSE200_black_CSE200();
	this.instance_10.parent = this;
	this.instance_10.setTransform(295,294);

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(272));

	// lock
	this.instance_11 = new lib.Symbol11("synched",0);
	this.instance_11.parent = this;
	this.instance_11.setTransform(348,319.5,1,1,0,0,0,17,18.5);
	this.instance_11.alpha = 0;
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(237).to({_off:false},0).to({y:309.5,alpha:1},5).wait(30));

	// CTA
	this.button_1 = new lib.Symbol12();
	this.button_1.name = "button_1";
	this.button_1.parent = this;
	this.button_1.setTransform(321.5,624.4,1,1,0,0,0,84.5,20);
	this.button_1._off = true;
	new cjs.ButtonHelper(this.button_1, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get(this.button_1).wait(257).to({_off:false},0).wait(2).to({scaleX:0.9,scaleY:0.9},0).wait(2).to({scaleX:1,scaleY:1},0).wait(11));

	// shield
	this.instance_12 = new lib.Symbol10("synched",0);
	this.instance_12.parent = this;
	this.instance_12.setTransform(348.5,337.5,0.109,0.109,0,0,0,61.8,64);
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(230).to({_off:false},0).to({regX:61.6,regY:61.7,scaleX:1.2,scaleY:1.2},6).wait(1).to({regX:61.5,scaleX:1.09,scaleY:1.09},0).wait(35));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(189,276,1440,800);
// library properties:
lib.properties = {
	id: 'C7460226EE5C4A41A91D0FC75828EA79',
	width: 700,
	height: 700,
	fps: 18,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"../assets/img/Desktop_LPhoneOn.png?1519141358619", id:"Desktop_LPhoneOn"},
		{src:"../assets/img/Desktop_RTabletOn.png?1519141358619", id:"Desktop_RTabletOn"},
		{src:"../assets/img/illu_CSE200_black_CSE200.png?1519141358619", id:"illu_CSE200_black_CSE200"},
		{src:"../assets/img/LLaptop_Off_Desktop.png?1519141358619", id:"LLaptop_Off_Desktop"},
		{src:"../assets/img/LLaptop_On_Desktop.png?1519141358619", id:"LLaptop_On_Desktop"},
		{src:"../assets/img/LockIcon.png?1519141358619", id:"LockIcon"},
		{src:"../assets/img/RLaptop_Off_Desktop.png?1519141358619", id:"RLaptop_Off_Desktop"},
		{src:"../assets/img/RLaptop_On_Desktop.png?1519141358619", id:"RLaptop_On_Desktop"},
		{src:"../assets/img/TV1Device.png?1519141358619", id:"TV1Device"},
		{src:"../assets/img/TV2Devices.png?1519141358619", id:"TV2Devices"},
		{src:"../assets/img/TV4Devices.png?1519141358619", id:"TV4Devices"},
		{src:"../assets/img/TVOff.png?1519141358619", id:"TVOff"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['C7460226EE5C4A41A91D0FC75828EA79'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;